require 'rails_helper'

RSpec.feature "PostsScrollings", type: :feature do
  describe "Posts must appear after scrolling to bottom" do
    before do
      100.times do
        Post.create(title: Faker::Book.title, description: Faker::Lorem.paragraph_by_chars)
      end
      visit posts_path
    end

    it "loads more posts while scroll to bottom", js: true do
      posts = Post::PAGINATE

      while posts < Post.all.count
        expect(page).to have_css('.post', :count => posts)
        page.execute_script('window.scrollBy(0, $(document).height())')
        sleep 0.5
        posts += Post::PAGINATE
      end
    end
  end
end
