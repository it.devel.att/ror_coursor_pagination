# README

Используется:

* Ruby version 2.5.1
* Bundler version 2.0.2
* Postgres for Database


В папке проекта выполнить 
```
bundle
rake db:create
rake db:migrate
rake db:seed
```
Есть один e2e тест на проверку того что новые посты грузятся при скроле вниз (для прогона теста в папке проекта выполнить)
```
rspec
```
