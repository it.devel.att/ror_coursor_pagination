class Api::V1::PostsController < ApiController
  before_action :set_last_post_id, only: [:index]

  def index
    @posts = Post.all.reverse_order.limit(Post::PAGINATE).where("id < ?", @last_post_id)
    render :json => @posts
  end

  private

  def set_last_post_id
    @last_post_id = params[:last_post_id].to_i
  end
end