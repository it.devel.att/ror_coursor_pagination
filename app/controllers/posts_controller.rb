class PostsController < ApplicationController
  before_action :set_post, only: [:show]

  def index
    if params[:last_post_id].nil?
      redirect_to posts_path(:last_post_id => Post.last.id + 1)
    end
    @posts = Post.all.reverse_order.where("id >= ?", params[:last_post_id].to_i - Post::PAGINATE)
  end

  def show
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)

    if @post.save
      redirect_to posts_path(:last_post_id => params[:last_post_id].nil? ? Post.last.id + 1 : params[:last_post_id])
    else
      render :new
    end
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :description)
  end
end
