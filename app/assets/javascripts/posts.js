$(document).on('turbolinks:load', function () {

    addEventListener("popstate", function (e) {
        window.location.replace(document.URL);
    }, false);

    $('.posts.index').ready(function () {
        if ($('#postsList').length > 0) {

            function postBlock(id, title, description) {
                return '<div class="col-12 post" data-id="' + id + '">' +
                    '<div class="card bg-light mb-3">' +
                    '<div class="card-header">' + id + ' ' + title + '</div>' +
                    '<div class="card-body">' +
                    '<p class="card-text">' + description + '</p>' +
                    '<a href="/posts/' + id + '" class="btn btn-success">Show</a> ' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }

            function getPostsAndAppendToHTML(postId) {
                $.ajax({
                    url: '/api/v1/posts?last_post_id=' + postId,
                    cache: true,
                    success: function (posts) {
                        $.each(posts, function (index, post) {
                            $('#postsList').append(postBlock(post.id, post.title, post.description));
                        })
                    }
                });
            }

            $(window).scroll(function () {
                let top_of_element = $("div.post:last").offset().top;
                let bottom_of_element = $("div.post:last").offset().top + $("div.post:last").outerHeight();
                let bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                let top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
                    let params = new window.URLSearchParams(window.location.search);
                    let lastPostId = +params.get('last_post_id');
                    let currentLastPostId = $("div.post:last").data('id');

                    window.history.pushState(null, '', '/posts?last_post_id=' + currentLastPostId);
                    if (currentLastPostId !== lastPostId) {
                        getPostsAndAppendToHTML(currentLastPostId);
                    }
                }
            });
        }
    });
});