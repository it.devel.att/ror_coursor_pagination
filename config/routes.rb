Rails.application.routes.draw do
  root 'posts#index'
  resources :posts
  namespace :api do
    namespace :v1 do
      resources :posts, only: [:index]
    end
  end
end
